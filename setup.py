import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open('requirements.txt') as requirements_file:
    install_requirements = requirements_file.read().splitlines()

setup(
    name="pynetid",
    version="0.0.3",
    author="Drew Stinnett",
    author_email="drew.stinnett@duke.edu",
    description=("Interact with NetID services"),
    license="BSD",
    keywords="netid password api cli",
    packages=find_packages(),
    install_requires=install_requirements,
    scripts=[
        'scripts/netid-change-password.py',
        'scripts/generate-password.py'
    ],
    long_description=read('README.md'),
)
