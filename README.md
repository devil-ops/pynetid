# Description

Misc. tools and  python libraries to help in interacting with Duke NetID
services

# Tools

## netid-change-password.py

Used to store and retrieve your password from a secure location, and calls a
web service to change it

Random passwords are generated using a password generator compatible with NetID
passwords

```
usage: netid-change-password.py [-h] [-c CURRENT_PASSWORD] [-n NEW_PASSWORD]
                                [-l PASSWORD_LENGTH] [--unsafe]
                                {shell,lastpass,vault} ... netid

Change your NetID Password

positional arguments:
  {shell,lastpass,vault}
                        Storage Scheme
    shell               Password just prints to the screen
    lastpass            Password is retrieved and written to lastpass
    vault               Password is retrieved and written to a HashiCorp Vault
  netid                 NetID to change

optional arguments:
  -h, --help            show this help message and exit
  -c CURRENT_PASSWORD, --current-password CURRENT_PASSWORD
                        Current Password
  -n NEW_PASSWORD, --new-password NEW_PASSWORD
                        New Password. Omit for random
  -l PASSWORD_LENGTH, --password-length PASSWORD_LENGTH
                        Length of randomly generated password
  --unsafe              Unsafe printing of passwords
```

### About Storage Backends

#### Shell

This is more of a 'null' backend, just printing the new password to the screen.

#### Lastpass

Duh, lastpass storage.  This requires you to have the 'lpass' command in your PATH.  This tool can be installed from:

[https://github.com/lastpass/lastpass-cli](https://github.com/lastpass/lastpass-cli)

#### Vault

Hashicorp Vault storage.  Currently you need the vault binary in your PATH.  This can be downloaded from:

[https://www.vaultproject.io/](https://www.vaultproject.io/)

Be sure to have the correct ENV variables exported in order for this to work (VAULT\_ADDR, VAULT\_TOKEN, etc)

