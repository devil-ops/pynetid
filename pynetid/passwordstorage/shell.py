from pynetid.passwordstorage import StorageBase


class ShellStorage(StorageBase):
    def __init__(self, netid):
        self.netid = netid
        super().__init__()

    def change_password(self, current_password, new_password):
        return self.do_change_password(self.netid, current_password,
                                       new_password)
