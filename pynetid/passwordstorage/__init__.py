import requests
import sys


class StorageBase(object):

    def __init__(self):
        self.netid_ws_url = 'https://idms-web-ws.oit.duke.edu/netid-ws'
        return None

    def do_change_password(self, netid, current_password, new_password):
        """
        This is the real netid password changing right here

        PUT the XML data to:
          https://idms-web-ws.oit.duke.edu/netid-ws/servicesRest/netids/$netid
        with your netid:password in the basic auth, like below

        curl -X PUT --user 'hiro:oldpassword’ -d '$XML_BLOB' \
          https://idms-web-ws.oit.duke.edu/netid-ws/servicesRest/netids/hiro
        """

        xml_template = """
<WsNetIDUpdate>
  <WsNetIDToUpdate>
    <ChangePassword>true</ChangePassword>
    <ChangePasswordDetails>
      <Password>%(password)s</Password>
    </ChangePasswordDetails>
  </WsNetIDToUpdate>
</WsNetIDUpdate>
        """
        data = {'password': new_password}
        put_data = xml_template % data

        url = '%s/servicesRest/netids/%s' % (self.netid_ws_url, netid)
        result = requests.put(url, data=put_data,
                              auth=(netid, current_password))

        # 201 is success, see:
        #  https://wiki.duke.edu/display/oitidms/NetID+Framework+Redesign
        if result.status_code == 201:
            return True
        else:
            sys.stderr.write("Password change unsuccessful, see below:\n")
            sys.stderr.write(result.text + "\n")
            return None
