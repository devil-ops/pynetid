from pynetid.passwordstorage import StorageBase
from pynetid.helpers import which
import os
import subprocess


class LastpassStorage(StorageBase):
    def __init__(self, netid, uniqueid):
        self.netid = netid
        self.uniqueid = uniqueid
        self.lpass_cmd = which('lpass')
        if not self.lpass_cmd:
            raise Exception(
                "No 'lpass' binary found, make sure it's in your $PATH")
        # Do an lpass sync to make sure we are unlocked
        self.lpass_sync()
        super().__init__()

    def lpass_sync(self):
        return os.system('{} sync'.format(self.lpass_cmd))

    def create_temp_store(self, new_password):
        """
        Create a temporary store location in lastpass for the new password
        """
        import secrets
        tmp_name = "%s-%s" % (self.uniqueid, secrets.token_hex(16))
        p = subprocess.Popen([
            self.lpass_cmd,
            'add',
            '--non-interactive',
            '--password',
            tmp_name,
        ], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        p.stdin.write(str.encode("%s\n" % new_password))
        p.communicate()[0]
        p.stdin.close()

        self.lpass_sync()
        return tmp_name

    def lpass_rm(self, entry_id):
        """
        Remove an lpass entry
        """
        print("Removing {}".format(entry_id))
        p = subprocess.Popen([
            self.lpass_cmd,
            'rm',
            entry_id
        ], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        p.communicate()[0]

        self.lpass_sync()
        return True

    def lpass_change_pw(self, new_password):
        """
        Set the password of an existing lpass entry
        """
        p = subprocess.Popen([
            self.lpass_cmd,
            'edit',
            '--non-interactive',
            '--password',
            self.uniqueid,
        ], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        p.stdin.write(str.encode("%s\n" % new_password))
        p.communicate()[0]
        p.stdin.close()

        self.lpass_sync()
        return True

    def lpass_existing_pw(self):
        """
        Return the existing password from lastpass
        """
        out = subprocess.check_output([
            self.lpass_cmd,
            'show',
            self.uniqueid,
            '--password'
        ]).decode("utf-8")
        out = out.strip()

        if "\n" in str(out):
            raise Exception('Found a newline in password, bombing')
        return out

    def change_password(self, current_password, new_password):

        # If given a null current password, yank the password out of lpass
        if not current_password:
            current_password = self.lpass_existing_pw()

        tmp_store = self.create_temp_store(new_password)

        print("Saving new password temporarity to {}".format(tmp_store))

        # Do the actual password change upstream
        if self.do_change_password(self.netid,
                                   current_password, new_password):
            # Save it to the lpass storage bit
            self.lpass_change_pw(new_password)
            self.lpass_rm(tmp_store)
            return True
        else:
            return None
#   def change_password(self, current_password, new_password):
#       return self.do_change_password(self.netid, current_password,
#                                      new_password)
