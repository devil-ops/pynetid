from pynetid.passwordstorage import StorageBase
from pynetid.helpers import which
import subprocess


class VaultStorage(StorageBase):
    def __init__(self, netid, path):
        self.netid = netid
        self.path = path
        self.vault_cmd = which('vault')
        if not self.vault_cmd:
            raise Exception(
                "No 'vault' binary found, make sure it's in your $PATH")
        # Do an vault sync to make sure we are unlocked
        super().__init__()

    def create_temp_store(self, new_password):
        """
        Create a temporary store location in vault for the new password
        """
        import secrets
        tmp_name = "%s-%s" % (self.path, secrets.token_hex(16))
        p = subprocess.Popen([
            self.vault_cmd,
            'write',
            tmp_name,
            'password=%s' % new_password
        ], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        p.stdin.write(str.encode("%s\n" % new_password))
        p.communicate()[0]
        p.stdin.close()

        return tmp_name

    def vault_rm(self, path):
        """
        Remove an vault entry
        """
        print("Removing {}".format(path))
        p = subprocess.Popen([
            self.vault_cmd,
            'delete',
            path
        ], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        p.communicate()[0]

        return True

    def vault_change_pw(self, new_password):
        """
        Set the password of an existing vault entry
        """
        p = subprocess.Popen([
            self.vault_cmd,
            'write',
            self.path,
            'password=%s' % new_password
        ], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        p.communicate()[0]

        return True

    def vault_existing_pw(self):
        """
        Return the existing password from vault
        """
        out = subprocess.check_output([
            self.vault_cmd,
            'read',
            '-field',
            'password',
            self.path,
        ]).decode("utf-8")
        out = out.strip()

        if "\n" in str(out):
            raise Exception('Found a newline in password, bombing')
        return out

    def change_password(self, current_password, new_password):

        # If given a null current password, yank the password out of vault
        if not current_password:
            current_password = self.vault_existing_pw()

        tmp_store = self.create_temp_store(new_password)

        print("Saving new password temporarity to {}".format(tmp_store))

        # Do the actual password change upstream
        if self.do_change_password(self.netid,
                                   current_password, new_password):
            # Save it to the vault storage bit
            self.vault_change_pw(new_password)
            self.vault_rm(tmp_store)
            return True
        else:
            return None
