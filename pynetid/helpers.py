def generate_netid_password(length=None):
    """
    Return a password generated to Duke NetID standards listed at:

    https://security.duke.edu/passwords
    """
    import random
    from grampg import PasswordGenerator

    if not length:
        length = int(random.randrange(16, 30))

    min_length = 16
    if length < min_length:
        raise Exception("Passwords must be at least %s characters" %
                        min_length)
    passwords = (PasswordGenerator({'special': list('*!_$#&%@^=~+-')}).of()
                 .at_least(2, 'numbers')
                 .at_least(2, 'letters')
                 .at_least(2, 'special')
                 .length(length)
                 .done())
    return [passwords.generate() for i in iter(range(1))][0]


def which(program):
    """
    Lovingly taken from:

    https://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
    """
    import os

    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None
