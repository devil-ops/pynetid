#!/usr/bin/env python3
import sys
import argparse
import requests
from getpass import getpass


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(description="This is my super sweet script")
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )
    parser.add_argument("-n", "--netid", help="NetID for public key")

    return parser.parse_args()


def main():
    args = parse_args()
    print(args)
    password = getpass("Password for %s: " % args.netid)

    data = requests.get(
        "https://idms-web-ws.oit.duke.edu/idm-ws/public/self",
        params={"attributes": "USR_UDF_SSH_PUBLIC_KEY"},
        auth=(args.netid, password),
    )
    data.raise_for_status()

    for userinfo in data.json()["userQueryResult"]["users"]:
        print(
            "User %s has %s keys"
            % (
                userinfo["userId"],
                len(userinfo["attributes"]["USR_UDF_SSH_PUBLIC_KEY"]),
            )
        )
        for key in userinfo["attributes"]["USR_UDF_SSH_PUBLIC_KEY"]:
            print(key)

    return 0


if __name__ == "__main__":
    sys.exit(main())
