#!/usr/bin/env python3

import sys
import argparse
from pynetid.passwordstorage.shell import ShellStorage
from pynetid.passwordstorage.lastpass import LastpassStorage
from pynetid.passwordstorage.vault import VaultStorage
from pynetid.helpers import generate_netid_password
import getpass


def parse_args():
    parser = argparse.ArgumentParser(
        description='Change your NetID Password')

    parser.add_argument('-c', '--current-password', type=str,
                        help='Current Password')
    parser.add_argument('-n', '--new-password', type=str,
                        help='New Password.  Omit for random')
    parser.add_argument('-l', '--password-length', type=int, default=25,
                        help='Length of randomly generated password')
    parser.add_argument('--unsafe', action='store_true',
                        help="Unsafe printing of passwords")

    # Here are the different storage options for the password
    subparsers = parser.add_subparsers(dest='storage', help='Storage Scheme')
    shell_storage = subparsers.add_parser(
        'shell', help='Password just prints to the screen')

    lastpass_storage = subparsers.add_parser(
        'lastpass', help='Password is retrieved and written to lastpass')
    lastpass_storage.add_argument('uniqueid',
                                  help='Unique name or id in lastpass')
    vault_storage = subparsers.add_parser(
        'vault',
        help='Password is retrieved and written to a HashiCorp Vault')
    vault_storage.add_argument('path', help='Path to secret in vault')
    parser.add_argument('netid', type=str, help='NetID to change')
    return parser.parse_args()


def main():
    args = parse_args()
    if args.current_password:
        current_password = args.current_password
    elif ((not args.current_password) and (args.storage == 'shell')):
        current_password = getpass.getpass('Current Password:')
    else:
        current_password = None

    if args.new_password:
        new_password = args.new_password
    else:
        new_password = generate_netid_password(length=args.password_length)

    if args.storage == 'shell':
        store = ShellStorage(args.netid)
        print("Changing password via shell for {}".format(args.netid))
    elif args.storage == 'lastpass':
        store = LastpassStorage(args.netid, args.uniqueid)
        print("Changing password via lastpass for {}".format(args.netid))
    elif args.storage == 'vault':
        store = VaultStorage(args.netid, args.path)
        print("Changing password via vault for {}".format(args.netid))
    else:
        sys.stderr.write("%s is not yet supported\n" % args.storage)
        sys.exit(2)

    if args.unsafe:
        print("Changing password to '{}'".format(new_password))

    pw_change = store.change_password(current_password, new_password)
    if pw_change:
        print("Password successfully changed")
    else:
        print("Password change failed")
    return 0


if __name__ == "__main__":
    sys.exit(main())
