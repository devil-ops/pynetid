#!/usr/bin/env python3
import sys

from pynetid.helpers import generate_netid_password


def main():
    print(generate_netid_password())
    return 0


if __name__ == "__main__":
    sys.exit(main())
