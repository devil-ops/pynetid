#!/usr/bin/env python3
import sys
import argparse
import requests
from getpass import getpass


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(description="This is my super sweet script")
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )
    parser.add_argument(
        "public_key",
        type=argparse.FileType("r"),
        help="SSH Public Key to upload",
        nargs="+",
    )
    parser.add_argument("-n", "--netid", help="NetID for public key", required=True)

    return parser.parse_args()


def main():
    args = parse_args()

    key_contents = []
    for public_key in args.public_key:
        key_contents.append(public_key.read().strip())
    password = getpass("Password for %s: " % args.netid)

    data = requests.post(
        "https://idms-web-ws.oit.duke.edu/idm-ws/public/self",
        headers={"Content-type": "application/json"},
        json={"USR_UDF_SSH_PUBLIC_KEY": key_contents},
        auth=(args.netid, password),
    )
    data.raise_for_status()
    if not data.json()["error"]:
        print("Success! 😁")
    else:
        print("Fail! 😓")

    return 0


if __name__ == "__main__":
    sys.exit(main())
